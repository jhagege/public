Author: Joachim Hagege

CREATE A NEW BRANCH:
git branch pizza

LIST ALL BRANCHES
git branch
  master
* pizza

SWITCH TO BRANCH
git checkout pizza

LIST BRANCHES AGAIN:
git branch
  master
* pizza

2/ MAKING CHANGES INSIDE YOUR NEW BRANCH
